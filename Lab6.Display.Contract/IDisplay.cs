﻿using System;
using System.Windows;

namespace Lab6.Display.Contract
{
    public interface IDisplay
    {
        string Text { set; }

        Window Window { get; }

        string Show { get; }
    }
}
