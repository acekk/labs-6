﻿using Lab6.ControlPanel.Contract;
using Lab6.MainComponent.Contract;
using PK.Container;
using System;
using System.Reflection;
using Lab6.MainComponent.Implementation;


namespace Lab6.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1

        public static Func<IContainer> ContainerFactory = () => { return new PK.Container.MyContainer(); };

        public static Assembly ControlPanelSpec = Assembly.GetAssembly(typeof(IControlPanel));
        public static Assembly ControlPanelImpl = Assembly.GetAssembly(typeof(ControlPanel.Implementation.ControlPanel));

        public static Assembly MainComponentSpec = Assembly.GetAssembly(typeof(IMyjnia));
        public static Assembly MainComponentImpl = Assembly.GetAssembly(typeof(Myjnia));

        #endregion
    }
}
