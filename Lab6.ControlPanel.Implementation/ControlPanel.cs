﻿using Lab6.ControlPanel.Contract;
using Lab6.MainComponent.Contract;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Lab6.ControlPanel.Implementation
{
    public class ControlPanel : IControlPanel
    {
        private INotifyPropertyChanged notify;
        private IMyjnia myjnia;

        public ControlPanel(IMyjnia myjnia, INotifyPropertyChanged notify)
        {
            this.myjnia = myjnia;
            this.notify = notify;
        }

        public void Wylacz()
        {
            myjnia.Wylacz();
        }

        public void Myj()
        {
            myjnia.Myj();
        }

        public void Uruchom_szczoty()
        {
            myjnia.Uruchom_szczoty();
        }

        public void Wlacz_wode()
        {
            myjnia.Wlacz_wode();
        }

        public Window Window
        {
            get { return new ControlPanelForm(this); }
        }
    }
}
