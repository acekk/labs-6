﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab6.MainComponent.Contract;
using Lab6.Display.Contract;

namespace Lab6.MainComponent.Implementation
{
    public class Myjnia : IMyjnia
    {
        private IDisplay display;

        public Myjnia(IDisplay display)
        {
            this.display = display;
            display.Text = "GOTOWY";
            var c = display.Show;
        }

        public void Uruchom_szczoty()
        {
            display.Text = "SZczoty poszly";
            var c = display.Show;
        }

        public void Wlacz_wode()
        {
            display.Text = "Woda leci";
            var c = display.Show;
        }

        public void Myj()
        {
            display.Text = "MYJE";
            var c = display.Show;
        }

        public void Wylacz()
        {
            display.Text = "Wylaczono";
            var c = display.Show;
        }
    }
}
