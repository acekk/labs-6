﻿Imports Lab6.Display.Contract

Public Class Display
    Implements IDisplay

    Private _window As DisplayForm
    Private _model As DisplayViewModel

    Public Sub New()
        _window = New DisplayForm()
        _model = New DisplayViewModel()
        _model = _window.DataContext
    End Sub

    Public WriteOnly Property Text As String Implements IDisplay.Text
        Set(value As String)
            _model.Text = value
        End Set
    End Property

    Public ReadOnly Property Window As Window Implements IDisplay.Window
        Get
            Return _window
        End Get
    End Property

End Class
