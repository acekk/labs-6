﻿using Lab6.ControlPanel.Contract;
using Lab6.Display.Contract;
using Lab6.Infrastructure;
using PK.Container;
using System;
using System.Windows;

namespace Lab6.Main
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        IDisplay display;
        IControlPanel controlPanel;

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            var container = Configuration.ConfigureApp();

            display = container.Resolve<IDisplay>();
            display.Window.Show();
            display.Text = "Test";

            controlPanel = container.Resolve<IControlPanel>();
            controlPanel.Window.Show();
        }
    }
}
